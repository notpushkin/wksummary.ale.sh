# [wksummary.ale.sh](https://wksummary.ale.sh/)

Review summaries for WaniKani. See [this post][1] for explanation.

[1]: https://community.wanikani.com/t/updates-to-lessons-reviews-and-extra-study-are-live/61049/399?u=notpushkin


## Development quickstart

```sh
pnpm install
pnpm run dev
```
