import { writable } from "svelte/store";

export const token = writable(localStorage.getItem("token") || null);
token.subscribe((value) => {
	if (value) {
		localStorage.setItem("token", value);
	} else {
		localStorage.removeItem("token");
	}
});

const lastReviewRaw = localStorage.getItem("lastReview");
export const lastReview = writable(lastReviewRaw ? new Date(lastReviewRaw) : null);
lastReview.subscribe((value: Date) => {
	if (value) {
		localStorage.setItem("lastReview", value.toISOString());
	} else {
		localStorage.removeItem("lastReview");
	}
});
