export async function getReviews(token: string, after: string) {
	const reviews = await fetch(`https://api.wanikani.com/v2/reviews?updated_after=${after}`, {
		headers: { Authorization: `Bearer ${token}` },
	}).then((req) => req.json());

	const subjectIds = reviews.data.map(({ data }) => data.subject_id);
	const subjects = await fetch(`https://api.wanikani.com/v2/subjects?ids=${subjectIds.join(",")}`, {
		headers: { Authorization: `Bearer ${token}` },
	}).then((req) => req.json());
	const subjectsMap = new Map(subjects.data.map((subject) => [subject.id, subject]));

	return reviews.data.map((review) => {
		const subject = subjectsMap.get(review.data.subject_id);
		return {
			correct: !(review.data.incorrect_meaning_answers || review.data.incorrect_reading_answers),
			href: subject.data.document_url,
			kind: subject.object,
			characters: subject.data.characters,
			imageSrc: subject.data.character_images ? subject.data.character_images.filter(
				(i) => i.content_type == "image/png",
			)[0].url : null,
			meaning: subject.data.meanings[0].meaning,
		};
	});
}
